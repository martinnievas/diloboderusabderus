import imp
import time
import sys
import zmq
import pickle
import numpy as np

from freenove import FreenoveHexapodKit
from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.zmq_interface import CONFIGURATION
from diloboderusabderus.hexapod_model import Hexapod


if __name__ == '__main__':
    morphology_config = imp.load_source('', sys.argv[1])

    robot = FreenoveHexapodKit()
    kinematic_model = Hexapod(np.array((0, 0, 0, 1)),
                              np.array((0, 0, 0, 1)),
                              morphology_config)

    ctx = zmq.Context()
    configuration_sub = ctx.socket(zmq.SUB)
    configuration_sub.connect("tcp://127.0.0.1:5556")
    configuration_sub.setsockopt(zmq.SUBSCRIBE, CONFIGURATION)
    all_angles = []
    while True:
        try:
            key, data = configuration_sub.recv_multipart()
            config = pickle.loads(data)
            angles = kinematic_model.inverse_kinematics(config)
            print('New angles: ', angles)
            all_angles.append(angles)
        except KeyboardInterrupt:
            break

    for angles in all_angles:
        robot.send_full_angles(angles)
        time.sleep(0.01)
