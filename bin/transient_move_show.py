import imp
import sys
import time
import zmq
import pickle
import numpy as np

from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.trajectory_generator import TransientTrajectoryGenerator
import diloboderusabderus.body_trajectory_generator as tgen
from diloboderusabderus.hexapod_model import Hexapod
from diloboderusabderus.gaits.wave import ForwardWaveGait


if __name__ == '__main__':
    morphology_config = imp.load_source('', sys.argv[1])
    stride = morphology_config.stride
    distance = 10*stride
    body_height = (morphology_config.legs_lengths[1] + morphology_config.legs_lengths[2])*.7
    central_legs_point_position = [
            np.array((morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5, 0, -body_height, 1)),
            np.array((morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5, 0, -body_height, 1)),
            np.array((morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5, 0, -body_height, 1)),
            np.array((morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5, 0, -body_height, 1)),
            np.array((morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5, 0, -body_height, 1)),
            np.array((morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5, 0, -body_height, 1)),
            ]
    initial_config = RobotConfiguration(
                                    body_position=np.array((0, 0, body_height, 1)),
                                    body_rotation=np.array((0, 0, 0, 1)),
                                    support_pattern=[0, 1, 2, 3, 4, 5],
                                    legs_point_position=central_legs_point_position
                                    )

    final_config = RobotConfiguration(
                                    body_position=np.array((0, stride/3, body_height, 1)),
                                    body_rotation=np.array((0, 0, 0, 1)),
                                    support_pattern=[0, 1, 2, 3, 4, 5],
                                    legs_point_position=central_legs_point_position
                                    )

    distance = 5*stride
    hexapod = Hexapod(initial_config.body_position, initial_config.body_rotation, morphology_config)
    gait = ForwardWaveGait(hexapod, initial_config, central_legs_point_position, distance, beta=6, stride=stride)
    trajectory_generator = TransientTrajectoryGenerator(
                                    hexapod,
                                    initial_config,
                                    gait.base_config,
                                    tgen.triangular_trajectory_gen,
                                    tgen.linear_trajectory_gen,
                                    tgen.body_linear_trajectory,
                                    )

    context = zmq.Context()
    socket_pub = context.socket(zmq.PUB)
    socket_pub.bind('tcp://127.0.0.1:5556')
    velocity = 30

    socket_pub.send_multipart([b'configuration', pickle.dumps(trajectory_generator.initial_config)])
    time.sleep(2)
    for config in trajectory_generator.get_config():
        socket_pub.send_multipart([b'configuration', pickle.dumps(config)])
        time.sleep(1/velocity)
