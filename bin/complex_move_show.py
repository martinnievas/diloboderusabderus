import imp
import numpy as np
import sys
import time

import diloboderusabderus.body_trajectory_generator as tgen
from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.trajectory_generator import TrajectoryGenerator, TransientTrajectoryGenerator
from diloboderusabderus.gaits.wave import ForwardWaveGait
from diloboderusabderus.gaits.spinning_wave import SpinningWaveGait
from diloboderusabderus.zmq_interface import RobotConfigurationPublisher
from diloboderusabderus.hexapod_model import Hexapod


if __name__ == '__main__':
    morphology_config = imp.load_source('', sys.argv[1])
    stride = morphology_config.stride

    body_height = (morphology_config.legs_lengths[1] + morphology_config.legs_lengths[2])*.7

    central_legs_point_position = [
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            ]

    initial_position = np.array((0, 0, body_height, 1))
    rotation = np.array((0, 0, 0, 1))
    initial_config = RobotConfiguration(
                                    body_position=initial_position.copy(),
                                    body_rotation=rotation.copy(),
                                    support_pattern=[0, 1, 2, 3, 4, 5],
                                    legs_point_position=central_legs_point_position
                                    )
    pub = RobotConfigurationPublisher()
    velocity = 30

    # From steady position to first gait position
    distance = 5*stride
    hexapod = Hexapod(initial_config.body_position, initial_config.body_rotation, morphology_config)
    gait = ForwardWaveGait(hexapod, initial_config, central_legs_point_position, distance, beta=6, stride=stride)
    transient_gen = TransientTrajectoryGenerator(
                                    hexapod,
                                    initial_config,
                                    gait.base_config,
                                    tgen.triangular_trajectory_gen,
                                    tgen.linear_trajectory_gen,
                                    tgen.body_linear_trajectory,
                                    )

    pub.blocking_send(initial_config)
    time.sleep(2)
    for config in transient_gen.get_config():
        pub.blocking_send(config)
        time.sleep(1/velocity)
    last_config = config.copy()

    # Lineal gait movement
    hexapod = Hexapod(last_config.body_position, last_config.body_rotation, morphology_config)
    trajectory_generator = TrajectoryGenerator(
                                    hexapod,
                                    last_config,
                                    central_legs_point_position,
                                    tgen.triangular_trajectory_gen,
                                    tgen.linear_trajectory_gen,
                                    tgen.body_linear_trajectory,
                                    ForwardWaveGait,
                                    distance,
                                    beta=6,
                                    stride=stride,
                                    step_multiplier=20
                                    )

    for config in trajectory_generator.get_config():
        pub.blocking_send(config)
        time.sleep(1/velocity)
    last_config = config.copy()

    # From last gait position to first spinning gait position
    stride = np.radians(morphology_config.angle_stride)
    distance = np.radians(90)
    hexapod = Hexapod(last_config.body_position, last_config.body_rotation, morphology_config)
    gait = SpinningWaveGait(hexapod, last_config, central_legs_point_position, distance, beta=6, stride=stride)
    transient_gen = TransientTrajectoryGenerator(
                                    hexapod,
                                    last_config,
                                    gait.base_config,
                                    tgen.triangular_trajectory_gen,
                                    tgen.linear_trajectory_gen,
                                    tgen.body_linear_trajectory,
                                    )

    for config in transient_gen.get_config():
        pub.blocking_send(config)
        time.sleep(1/velocity)
    last_config = config.copy()

    # Spinning movement
    hexapod = Hexapod(last_config.body_position, last_config.body_rotation, morphology_config)
    trajectory_generator = TrajectoryGenerator(
                                    hexapod,
                                    last_config,
                                    central_legs_point_position,
                                    tgen.triangular_trajectory_gen,
                                    tgen.linear_trajectory_gen,
                                    tgen.body_linear_trajectory,
                                    SpinningWaveGait,
                                    distance,
                                    beta=6,
                                    stride=stride,
                                    step_multiplier=20
                                    )

    for config in trajectory_generator.get_config():
        pub.blocking_send(config)
        time.sleep(1/velocity)
    last_config = config.copy()

    # From last gait position to first lineal gait position
    stride = morphology_config.stride
    distance = 5*stride
    hexapod = Hexapod(last_config.body_position, last_config.body_rotation, morphology_config)
    gait = ForwardWaveGait(hexapod, last_config, central_legs_point_position, distance, beta=6, stride=stride)
    transient_gen = TransientTrajectoryGenerator(
                                    hexapod,
                                    last_config,
                                    gait.base_config,
                                    tgen.triangular_trajectory_gen,
                                    tgen.linear_trajectory_gen,
                                    tgen.body_linear_trajectory,
                                    )

    for config in transient_gen.get_config():
        pub.blocking_send(config)
        time.sleep(1/velocity)
    last_config = config.copy()

    # Lineal gait movement
    hexapod = Hexapod(last_config.body_position, last_config.body_rotation, morphology_config)
    trajectory_generator = TrajectoryGenerator(
                                    hexapod,
                                    last_config,
                                    central_legs_point_position,
                                    tgen.triangular_trajectory_gen,
                                    tgen.linear_trajectory_gen,
                                    tgen.body_linear_trajectory,
                                    ForwardWaveGait,
                                    distance,
                                    beta=6,
                                    stride=stride,
                                    step_multiplier=20
                                    )

    for config in trajectory_generator.get_config():
        pub.blocking_send(config)
        time.sleep(1/velocity)
