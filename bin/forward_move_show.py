import imp
import numpy as np
import sys
import time

import diloboderusabderus.gaits.wave as wave_gait
import diloboderusabderus.body_trajectory_generator as tgen
from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.trajectory_generator import TrajectoryGenerator
from diloboderusabderus.zmq_interface import RobotConfigurationPublisher
from diloboderusabderus.hexapod_model import Hexapod


if __name__ == '__main__':
    morphology_config = imp.load_source('', sys.argv[1])
    stride = morphology_config.stride
    distance = 3*stride

    body_height = (morphology_config.legs_lengths[1] + morphology_config.legs_lengths[2])*.7
    position = np.array((0, 0, body_height, 1))
    rotation = np.array((0, 0, 0, 1))

    central_legs_point_position = [
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            np.array((
                morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
                0,
                -body_height,
                1)),
            ]
    initial_config = RobotConfiguration(
                                    body_position=position.copy(),
                                    body_rotation=rotation.copy(),
                                    support_pattern=[0, 1, 2, 3, 4, 5],
                                    legs_point_position=central_legs_point_position
                                    )
    hexapod = Hexapod(position, rotation, morphology_config)
    trajectory_generator = TrajectoryGenerator(
                                    hexapod,
                                    initial_config,
                                    central_legs_point_position,
                                    tgen.triangular_trajectory_gen,
                                    tgen.linear_trajectory_gen,
                                    tgen.body_linear_trajectory,
                                    wave_gait.ForwardWaveGait,
                                    distance,
                                    beta=10,
                                    stride=stride,
                                    step_multiplier=20
                                    )

    pub = RobotConfigurationPublisher()

    print(morphology_config.velocity)
    pub.blocking_send(trajectory_generator.gait.base_config)
    time.sleep(2)
    for config in trajectory_generator.get_config():
        pub.blocking_send(config)
        time.sleep(1/morphology_config.velocity)
