import zmq
import os
import pickle
import readline
import numpy as np
from cmd import Cmd

from diloboderusabderus.cartesian_coordinates import check_position, check_rotation
from diloboderusabderus.robot_configuration import RobotConfiguration


CMD_HISTORY = '.cmd_history'


class Prompt(Cmd):
    def __init__(self):
        super().__init__()
        context = zmq.Context()
        self.socket_pub = context.socket(zmq.PUB)
        self.socket_pub.bind('tcp://127.0.0.1:5556')

    def preloop(self):
        if os.path.exists(CMD_HISTORY):
            readline.read_history_file(CMD_HISTORY)

    def postloop(self):
        readline.write_history_file(CMD_HISTORY)

    def do_exit(self, inp):
        print('Bye')
        return True

    def help_exit(self):
        print('Exit console.')

    def do_send_robot_configuration(self, inp):
        try:
            config = eval(inp)
            robot_configuration = RobotConfiguration(
                                            np.array(config[0]),
                                            np.array(config[1]),
                                            config[2],
                                            [np.array(i) for i in config[3]],
                                            )
        except (SyntaxError, ValueError, TypeError) as e:
            print(str(e))
        else:
            self.socket_pub.send_multipart([b'configuration', pickle.dumps(robot_configuration)])

    def help_send_robot_configuration(self):
        print('Send RobotConfiguration. [[0, 0, 0, 1], [0, 0, 0, 1], [1, 2, 3], [[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]]]')

    def do_send_angles(self, inp):
        try:
            angles = eval(inp)
            angles_rad = []
            for leg_angles in angles:
                angles_rad.append([np.radians(i) for i in leg_angles])
        except (SyntaxError, ValueError, TypeError) as e:
            print(str(e))
        else:
            self.socket_pub.send_multipart([b'angles', pickle.dumps(angles_rad)])

    def help_send_angles(self):
        print('Send Angles. [[90, 10, 0], [90, 10, 0], [90, 10, 0], [90, 10, 0], [90, 10, 0], [90, 10, 0]]')

    do_EOF = do_exit
    help_EOF = help_exit
    do_q = do_exit
    help_q = help_exit
    do_quit = do_exit
    help_quit = help_exit


if __name__ == '__main__':
    Prompt().cmdloop()
