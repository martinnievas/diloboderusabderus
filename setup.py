#!/usr/bin/env python

from setuptools import find_packages
from setuptools import setup

setup(
        name='Diloboderus Abderus',
        version='0.1',
        description='Hexapod robot simulator and controler',
        author='Joaquin de Andres',
        author_email='xcancerberox@gmail.com',
        packages=find_packages('src'),
        package_dir={'': 'src'},
        install_requires=['numpy', 'pyglet'],
        test_suite='tests'
        )
