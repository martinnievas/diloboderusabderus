import zmq
import pickle

from diloboderusabderus.mylogging import logger
from diloboderusabderus.robot_configuration import RobotConfiguration


CONFIGURATION = b'configuration'
ANGLES = b'angles'


class MultiTopicSubscriber():
    def __init__(self, topics, handlers):
        if not isinstance(topics, list):
            raise ValueError
        self.topics = topics

        if not isinstance(handlers, list):
            raise ValueError
        self.handlers = handlers

        context = zmq.Context()
        self.subscribers = []
        for topic in topics:
            self.subscribers.append(context.socket(zmq.SUB))
            self.subscribers[-1].connect("tcp://127.0.0.1:5556")
            self.subscribers[-1].setsockopt(zmq.SUBSCRIBE, topic)

    def non_blocking_rcv(self):
        read_ready, w, x = zmq.select(self.subscribers, [], [], timeout=0.01)

        for read_socket in read_ready:
            [key, rcv] = read_socket.recv_multipart()
            for index, topic in enumerate(self.topics):
                if key == topic:
                    data = pickle.loads(rcv)
                    try:
                        logger.info('New data from topic {} received {}'.format(topic, data))
                        self.handlers[index](data)
                    except (ValueError, TypeError) as e:
                        print(e)


class RobotConfigurationPublisher():
    def __init__(self):
        context = zmq.Context()
        self.socket_pub = context.socket(zmq.PUB)
        self.socket_pub.bind('tcp://127.0.0.1:5556')

    def blocking_send(self, config):
        self.socket_pub.send_multipart([CONFIGURATION,
                                        pickle.dumps(config)])
