import numpy as np

from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.mylogging import logger


class TrajectoryGenerator():
    def __init__(self,
                 kinematic_model,
                 initial_config,
                 central_legs_point_position,
                 transfer_trajectoy_gen, support_trajectory_gen, body_trajectory_gen,
                 gait, distance, beta, stride,
                 step_multiplier=100):

        self.step_multiplier = step_multiplier
        self.stride = stride

        self.gait = gait(kinematic_model, initial_config, central_legs_point_position, distance, stride, beta)

        self.body_trajectory_gen_function = body_trajectory_gen
        self.transfer_trajectory_gen_function = transfer_trajectoy_gen
        self.support_trajectory_gen_function = support_trajectory_gen

    def get_config(self):
        legs_trajectory_gen = [None]*6
        gait_generator = self.gait.get_config()

        steps_on_support = self.gait.beta * self.step_multiplier
        steps_on_transfer = (self.gait.full_cycle - self.gait.beta) * self.step_multiplier
        steps_on_half_cycle = self.gait.full_cycle/2 * self.step_multiplier

        last_config = self.gait.base_config.copy()

        logger.info('Initial configuration {}'.format(self.gait.base_config))
        gait_step = 0
        for new_config in gait_generator:
            logger.info('New configuration for gait step {}: {}'.format(gait_step, new_config))
            if gait_step == 0:
                body_trajectory_gen = self.body_trajectory_gen_function(
                                                        self.gait.base_config,
                                                        new_config,
                                                        steps_on_half_cycle)

                remaining_steps = self.gait.get_remaining_steps_on(0)
                for leg in range(6):
                    if leg in new_config.support_pattern:
                        legs_trajectory_gen[leg] = self.support_trajectory_gen_function(
                                                                    self.gait.base_config.legs_point_position[leg],
                                                                    new_config.legs_point_position[leg],
                                                                    remaining_steps[leg]*self.step_multiplier)
                    else:
                        legs_trajectory_gen[leg] = self.transfer_trajectory_gen_function(
                                                                    self.gait.base_config.legs_point_position[leg],
                                                                    new_config.legs_point_position[leg],
                                                                    remaining_steps[leg]*self.step_multiplier)

            else:
                if not np.allclose(last_config.body_position, new_config.body_position) or\
                   not np.allclose(last_config.body_rotation, new_config.body_rotation):
                    body_trajectory_gen = self.body_trajectory_gen_function(
                                                            last_config,
                                                            new_config,
                                                            steps_on_half_cycle)

                for support_leg in new_config.support_pattern:
                    if support_leg not in last_config.support_pattern:
                        legs_trajectory_gen[support_leg] = self.support_trajectory_gen_function(
                                                                    last_config.legs_point_position[support_leg],
                                                                    new_config.legs_point_position[support_leg],
                                                                    steps_on_support)
                for support_leg in last_config.support_pattern:
                    if support_leg not in new_config.support_pattern:
                        legs_trajectory_gen[support_leg] = self.transfer_trajectory_gen_function(
                                                                    last_config.legs_point_position[support_leg],
                                                                    new_config.legs_point_position[support_leg],
                                                                    steps_on_transfer)

            for micro_step in range(self.step_multiplier):
                logger.info('Micro step {}'.format(micro_step))
                logger.info('Next Body position, rotation')
                next_body_config = body_trajectory_gen.__next__()
                new_config.body_position = next_body_config.body_position.copy()
                new_config.body_rotation = next_body_config.body_rotation.copy()
                for leg in range(6):
                    logger.info('Next leg position {}'.format(leg))
                    new_config.legs_point_position[leg] = legs_trajectory_gen[leg].__next__()

                yield new_config
            last_config = new_config.copy()
            gait_step += 1
        logger.info('Last config generated: {}'.format(last_config))


class TransientTrajectoryGenerator():
    def __init__(self,
                 kinematic_model,
                 initial_config, final_config,
                 transfer_trajectoy_gen, support_trajectory_gen, body_trajectory_gen,
                 step_multiplier=100):

        self.kinematic_model = kinematic_model

        self.initial_config = initial_config.copy()
        self.final_config = final_config.copy()
        self.step_multiplier = step_multiplier

        self.body_trajectory_gen_function = body_trajectory_gen
        self.transfer_trajectory_gen_function = transfer_trajectoy_gen
        self.support_trajectory_gen_function = support_trajectory_gen

    def get_config(self):

        last_config = self.initial_config.copy()
        if not (np.allclose(self.initial_config.body_position, self.final_config.body_position)
                and np.allclose(self.initial_config.body_rotation, self.final_config.body_rotation)):
            for config in self.body_trajectory_gen_function(self.initial_config.copy(), self.final_config.copy()):
                new_config = self.kinematic_model.configuration_to_new_body_pose(
                                                                  last_config,
                                                                  config.body_position,
                                                                  config.body_rotation)
                yield new_config
                last_config = new_config

        for leg in range(6):
            for position in self.transfer_trajectory_gen_function(last_config.legs_point_position[leg],
                                                                  self.final_config.legs_point_position[leg]):
                last_config.legs_point_position[leg] = position.copy()
                yield last_config
