import pyglet
import numpy as np

import kctools.kcvisualizer as kcvisualizer
from kctools.kcvisualizer.models.kchexapod6dof import Hexapod6DOFVisualizerModel

from diloboderusabderus.mylogging import logger
from diloboderusabderus.cartesian_coordinates import CartesianCoordinates, check_position, check_rotation
from diloboderusabderus.hexapod_model import Hexapod
from diloboderusabderus.zmq_interface import MultiTopicSubscriber, CONFIGURATION, ANGLES


class DiloboderusAbderusVisualizer(kcvisualizer.KCVisualizer):
    def __init__(self, robot_configuration, morphology_config):
        # Models for KCVisualizer Scene
        world_axis = kcvisualizer.models.base.Axis(length=30)
        world_floor = kcvisualizer.models.base.Floor()
        # Model of hexapod
        hexapod_visualizer_model = Hexapod6DOFVisualizerModel(
                                            position=[0, 0, 0, 1],
                                            rotation=[0, 0, 0, 0],
                                            morphology=morphology_config,
                                            axis_visible=False,
                                            )
        super().__init__({'hexapod': hexapod_visualizer_model,
                          'world_axis': world_axis,
                          'floor': world_floor,
                          })

        self.interface = MultiTopicSubscriber(
                                [CONFIGURATION, ANGLES],
                                [self.set_new_configuration, self.set_new_angles])
        pyglet.clock.schedule(self.clock_handler_socket_rcv, 1/10)

        # Kinematic model
        self.robot_config = robot_configuration
        self.hexapod = Hexapod(robot_configuration.body_position,
                               robot_configuration.body_rotation,
                               morphology_config)

        self.set_new_configuration(self.robot_config)

    def clock_handler_socket_rcv(self, dt, f):
        self.interface.non_blocking_rcv()

    def set_new_configuration(self, configuration):
        self.robot_config = configuration

        angles = self.hexapod.inverse_kinematics(self.robot_config)
        angles = [[np.degrees(rad_angle) for rad_angle in data] for data in angles]

        self.models['hexapod'].position = self.robot_config.body_position[:3]
        self.models['hexapod'].rotation = [np.degrees(self.robot_config.body_rotation[0]),
                                           self.robot_config.body_rotation[1],
                                           self.robot_config.body_rotation[2],
                                           self.robot_config.body_rotation[3]]
        self.models['hexapod'].set_angles(angles)
        self.camera.center = list(self.robot_config.body_position[:3])

    def set_new_angles(self, angles):
        angles = [[np.degrees(rad_angle) for rad_angle in data] for data in angles]
        self.models['hexapod'].set_angles(angles)
