import numpy as np

from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.hexapod_model import Hexapod
from diloboderusabderus.gaits.base import Gait
from diloboderusabderus.mylogging import logger


class ForwardWaveGait(Gait):
    def __init__(self, kinematic_model, initial_config, central_legs_point_position, distance, stride, beta):
        height = initial_config.body_position[2]
        super().__init__(height, distance, stride, beta)

        self.kinematic_model = kinematic_model
        self.initial_config = initial_config

        config = initial_config.copy()
        config.legs_point_position = central_legs_point_position

        # Transfer legs point position
        transfer_position = self.kinematic_model.relative_to_absolute_position(np.array((0, self.stride/2, 0, 1)))
        self.transfer_configuration = self.kinematic_model.configuration_to_new_body_pose(
                                                                           config.copy(),
                                                                           transfer_position.copy(),
                                                                           config.body_rotation.copy()).copy()
        self.transfer_configuration.body_position = initial_config.body_position.copy()
        self.transfer_configuration.body_rotation = initial_config.body_rotation.copy()
        self.transfer_initial_position = self.transfer_configuration.legs_point_position

        # Support
        support_position = self.kinematic_model.relative_to_absolute_position(np.array((0, -self.stride/2, 0, 1)))
        self.support_configuration = self.kinematic_model.configuration_to_new_body_pose(
                                                                          config.copy(),
                                                                          support_position.copy(),
                                                                          config.body_rotation.copy()).copy()
        self.support_configuration.body_position = initial_config.body_position.copy()
        self.support_configuration.body_rotation = initial_config.body_rotation.copy()
        self.support_initial_position = self.support_configuration.legs_point_position

        # Leg phase config
        self.legs_phase[3] = 0
        self.legs_phase[4] = self.beta
        self.legs_phase[5] = 2*self.beta - self.full_cycle
        self.legs_phase[0] = int(self.full_cycle/2)
        self.legs_phase[1] = (self.legs_phase[0] + self.legs_phase[4]) % self.full_cycle
        self.legs_phase[2] = (self.legs_phase[0] + self.legs_phase[5]) % self.full_cycle
        logger.info('Phase for legs {}'.format(self.legs_phase))

        self.calculate_support_range()
        self.calculate_base_config()

        self.displacement = (self.full_cycle/2)*self.stride/self.beta
        self.body_position_displacement = np.array((0, self.displacement, 0, 1))
        self.body_rotation_displacement = np.array((0, 0, 0, 0))

    def calculate_base_config(self):
        support_pattern = self.support_pattern_for(0)
        remaining_steps = self.get_remaining_steps_on(0)

        legs_point_position = [None]*6
        for leg in range(6):
            new_position = self.initial_config.body_position.copy()

            if leg in support_pattern:
                already_done_steps = self.beta - remaining_steps[leg]

                new_position = self.kinematic_model.relative_to_absolute_position(
                                    np.array((0, already_done_steps * self.support_speed, 0, 1)))
                new_config = self.kinematic_model.configuration_to_new_body_pose(
                                                                  self.support_configuration.copy(),
                                                                  new_position,
                                                                  self.initial_config.body_rotation.copy())
            else:
                already_done_steps = (self.full_cycle - self.beta) - remaining_steps[leg]
                new_position = self.kinematic_model.relative_to_absolute_position(
                                    np.array((0, -already_done_steps * self.support_speed, 0, 1)))
                new_config = self.kinematic_model.configuration_to_new_body_pose(
                                                                  self.transfer_configuration.copy(),
                                                                  new_position,
                                                                  self.initial_config.body_rotation.copy())

            legs_point_position[leg] = new_config.legs_point_position[leg]

        self.base_config = RobotConfiguration(
                                    self.initial_config.body_position.copy(),
                                    self.initial_config.body_rotation.copy(),
                                    support_pattern,
                                    legs_point_position)
