from diloboderusabderus.cartesian_coordinates import check_position, check_rotation


def check_pattern(pattern):
    if not (isinstance(pattern, tuple) or isinstance(pattern, list)):
        raise TypeError('Pattern not tuple or list')

    if len(pattern) == 0:
        raise ValueError('Empty pattern')


class RobotConfiguration():
    def __init__(self, body_position, body_rotation,
                 support_pattern, legs_point_position):

        check_position(body_position)
        check_rotation(body_rotation)
        check_pattern(support_pattern)

        self.body_position = body_position
        self.body_rotation = body_rotation
        self.support_pattern = support_pattern
        self.legs_point_position = legs_point_position

    def __repr__(self):
        repre = '{} {} {}\n'.format(self.body_position,
                                    self.body_rotation,
                                    self.support_pattern)
        repre += 'Legs support: \n'
        for leg_point in self.legs_point_position:
            repre += '\t{}\n'.format(leg_point)
        return repre

    def copy(self):
        new_config = RobotConfiguration(self.body_position.copy(),
                                        self.body_rotation.copy(),
                                        list(self.support_pattern),
                                        [])
        for point_position in self.legs_point_position:
            new_config.legs_point_position.append(point_position.copy())
        return new_config
