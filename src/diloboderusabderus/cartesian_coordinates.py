import numpy as np

from kctools.kcengine import transformations


def check_position(position):
    if not isinstance(position, np.ndarray):
        raise TypeError('Position not an numpy array {}'.format(type(position)))

    if not (position.shape == (4,)):
        raise ValueError('Position not the correct shape {}'.format(position.shape))

    if not (position[3] == 1):
        raise ValueError('Position not homogeneous {}'.format(position))


def check_rotation(rotation):
    if not isinstance(rotation, np.ndarray):
        raise TypeError('Rotation not an numpy array {}'.format(type(rotation)))

    if not (rotation.shape == (4,)):
        raise ValueError('Rotation not the correct shape {}'.format(rotation.shape))

    norm = np.linalg.norm(np.array(rotation[1:4]))
    if not (norm == 1):
        raise ValueError('Rotation vector not normalized {}, {}'.format(rotation, norm))


class CartesianCoordinates():
    def __init__(self, position, rotation):
        check_position(position)
        check_rotation(rotation)

        self.position = position
        self.rotation = rotation

    def absolute_to_relative_position(self, absolute_position):
        check_position(absolute_position)

        translation = transformations.translation(
                            self.position*np.array([-1, -1, -1, 1]))
        rotation = transformations.rotation(
                            self.rotation[1:4],
                            -self.rotation[0]
                            )
        translated = np.dot(translation, absolute_position)
        relative_position = np.dot(rotation, translated)
        return relative_position

    def relative_to_absolute_position(self, relative_position):
        check_position(relative_position)

        translation = transformations.translation(self.position)
        rotation = transformations.rotation(
                            self.rotation[1:4],
                            self.rotation[0]
                            )
        rotated = np.dot(rotation, relative_position)
        absolute_position = np.dot(translation, rotated)
        return absolute_position
