"""
Hexapod Kinematic model. Closed loop chain using N Legs connected to ground.

"""
import numpy as np

from diloboderusabderus.leg_model import Leg
from diloboderusabderus.cartesian_coordinates import CartesianCoordinates
from diloboderusabderus.robot_configuration import RobotConfiguration


def distance_to_line(line_point1, line_point2):
    if line_point2[0] == line_point1[0]:
        distance = abs(line_point1[0])
    elif line_point2[1] == line_point1[1]:
        distance = abs(line_point1[1])
    else:
        a = (line_point2[1] - line_point1[1])/(line_point2[0] - line_point1[0])
        b = line_point2[1] - a * line_point2[0]
        distance = abs(b/(np.sqrt(a**2 + 1)))
    return distance


class Hexapod(CartesianCoordinates):
    def __init__(self, position, rotation, morphology_config):
        super().__init__(position, rotation)

        self.morphology_config = morphology_config

        self.legs = []
        for leg_config in morphology_config.legs_config:
            rotation = (np.radians(leg_config['rotation'][0]), *leg_config['rotation'][1:])
            self.legs.append(Leg(np.array(leg_config['position']),
                                 np.array(rotation),
                                 morphology_config.legs_lengths))

    def inverse_kinematics(self, configuration):
        angles = []

        self.position = configuration.body_position
        self.rotation = configuration.body_rotation

        for index, leg in enumerate(self.legs):
            leg_point_relative_leg = configuration.legs_point_position[index]
            if not leg.reachable(leg_point_relative_leg):
                raise ValueError('Leg imposible configuration {}'.format(configuration))

            solutions = leg.inverse_kinematics(leg_point_relative_leg)
            subchain_positive_solution = solutions.positive[1]

            angles.append((solutions.positive[0],
                           subchain_positive_solution.positive[0],
                           subchain_positive_solution.positive[1]))

        return angles

    def configuration_to_new_body_pose(self, configuration, new_body_position, new_body_rotation):

        hexapod_new_config = Hexapod(new_body_position, new_body_rotation, self.morphology_config)
        new_configuration = RobotConfiguration(new_body_position, new_body_rotation,
                                               configuration.support_pattern, configuration.legs_point_position)

        for index, leg_point in enumerate(configuration.legs_point_position):
            leg_point_body = self.legs[index].relative_to_absolute_position(leg_point)
            leg_point_world = self.relative_to_absolute_position(leg_point_body)
            leg_point_body_translated = hexapod_new_config.absolute_to_relative_position(leg_point_world)
            leg_point_translated = self.legs[index].absolute_to_relative_position(leg_point_body_translated)
            new_configuration.legs_point_position[index] = leg_point_translated

        return new_configuration

    def stability(self, configuration):
        support_pattern_dist = []
        lines_on_support_pattern = []

        clock_wise = [0, 3, 4, 5, 2, 1]
        first_leg = configuration.support_pattern[0]

        index = clock_wise.index(first_leg)
        reordered_clock_wise = []
        if index != 5:
            reordered_clock_wise += clock_wise[index + 1:]
        if index != 0:
            reordered_clock_wise += clock_wise[:index]
        reordered_clock_wise += [first_leg]

        for i in reordered_clock_wise:
            if i in configuration.support_pattern:
                lines_on_support_pattern.append([first_leg, i])
                first_leg = i

        print(lines_on_support_pattern)
        if len(lines_on_support_pattern) < 3:
            raise ValueError('Pattern is not stable')

        for line in lines_on_support_pattern:
            leg_0_point_to_body = self.legs[line[0]].relative_to_absolute_position(configuration.legs_point_position[line[0]])
            leg_1_point_to_body = self.legs[line[1]].relative_to_absolute_position(configuration.legs_point_position[line[1]])
            distance = distance_to_line(leg_0_point_to_body[:2], leg_1_point_to_body[:2])
            support_pattern_dist.append(distance)

        return np.min(support_pattern_dist)
