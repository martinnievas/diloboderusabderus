import unittest
import numpy as np

from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.gaits.tripod import GaitTripod
from diloboderusabderus.hexapod_model import legs_lengths


initial_body_position = np.array((0, 0, (legs_lengths[1] + legs_lengths[2])/1.3, 1))
initial_body_rotation = np.array((0, 0, 0, 1))
support_pattern = [0, 1, 2, 3, 4, 5]
stride = 10

center_legs_point_position = [
        np.array((legs_lengths[0]*1.5, 0, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, 0, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, 0, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, 0, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, 0, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, 0, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        ]
right_legs_point_position = [
        np.array((legs_lengths[0]*1.5,  stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, -stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5,  stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5,  stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, -stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5,  stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        ]
left_legs_point_position = [
        np.array((legs_lengths[0]*1.5, -stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5,  stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, -stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, -stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5,  stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        np.array((legs_lengths[0]*1.5, -stride/2, -(legs_lengths[1] + legs_lengths[2])/1.3, 1)),
        ]


class TestTripodGaitLongStrideMultiple(unittest.TestCase):
    def setUp(self):
        self.gait = GaitTripod(
                                initial_configuration=RobotConfiguration(
                                                        initial_body_position.copy(),
                                                        initial_body_rotation.copy(),
                                                        list(support_pattern),
                                                        list(center_legs_point_position)),
                                final_configuration=RobotConfiguration(
                                                        initial_body_position.copy() + np.array((0, 3*stride, 0, 0)),
                                                        initial_body_rotation.copy(),
                                                        list(support_pattern),
                                                        list(center_legs_point_position)),
                                stride=stride,
                                )
        self.distance = 3*stride

        self.expected = [
                # Initial step
                {
                    'steps': [0, 1, 2, 3, 4, 5],
                    'displacement': stride/2,
                    'support_pattern': [1, 3, 5],
                    'support_legs_point_position': left_legs_point_position,
                    'transfer_legs_point_position': right_legs_point_position,
                    },
                # Second step
                {
                    'steps': [6, 7, 8, 9, 10, 11],
                    'displacement': stride,
                    'support_pattern': [0, 2, 4],
                    'support_legs_point_position': right_legs_point_position,
                    'transfer_legs_point_position': left_legs_point_position,
                    },
                # Third step
                {
                    'steps': [12, 13, 14, 15, 16, 17],
                    'displacement': stride,
                    'support_pattern': [1, 3, 5],
                    'support_legs_point_position': left_legs_point_position,
                    'transfer_legs_point_position': right_legs_point_position,
                    },
                # Fourth step
                {
                    'steps': [18, 19, 20, 21, 22, 23],
                    'displacement': stride/2,
                    'support_pattern': [0, 2, 4],
                    'support_legs_point_position': center_legs_point_position,
                    'transfer_legs_point_position': center_legs_point_position,
                    },
                ]

    def test_legs_position(self):
        step = 0
        for config in self.gait.get_config():
            with self.subTest(i=step):
                for data_expected in self.expected:
                    if step in data_expected['steps']:
                        break

                for leg in range(6):
                    if leg in config.support_pattern:
                        leg_point_expected = data_expected['support_legs_point_position'][leg]
                    else:
                        leg_point_expected = data_expected['transfer_legs_point_position'][leg]

                    np.testing.assert_almost_equal(leg_point_expected, config.legs_point_position[leg])
            step += 1

    def Test_support_pattern(self):
        index = 0
        for config in self.gait.get_config():
            with self.subTest(i=index):
                self.assertEqual(self.expected[index]['fin_support_pattern'], config.support_pattern)
            index += 1


if __name__ == '__main__':
    unittest.main()
