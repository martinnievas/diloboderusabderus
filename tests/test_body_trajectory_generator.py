import unittest
import numpy as np

import diloboderusabderus.body_trajectory_generator as btg
from diloboderusabderus.robot_configuration import RobotConfiguration


class TestLinearTrajectoryGenerator(unittest.TestCase):
    def test_foward_x(self):
        initial_position = np.array((0, 0, 0, 1))
        final_position = np.array((10, 0, 0, 1))
        steps = 10
        points = [
                np.array((1, 0, 0, 1)),
                np.array((2, 0, 0, 1)),
                np.array((3, 0, 0, 1)),
                np.array((4, 0, 0, 1)),
                np.array((5, 0, 0, 1)),
                np.array((6, 0, 0, 1)),
                np.array((7, 0, 0, 1)),
                np.array((8, 0, 0, 1)),
                np.array((9, 0, 0, 1)),
                np.array((10, 0, 0, 1)),
                ]
        index = 0
        for gen_point in btg.linear_trajectory_gen(initial_position, final_position, steps):
            np.testing.assert_almost_equal(points[index], gen_point)
            index += 1

    def test_foward_y(self):
        initial_position = np.array((0, 0, 0, 1))
        final_position = np.array((0, 10, 0, 1))
        steps = 10
        points = [
                np.array((0, 1, 0, 1)),
                np.array((0, 2, 0, 1)),
                np.array((0, 3, 0, 1)),
                np.array((0, 4, 0, 1)),
                np.array((0, 5, 0, 1)),
                np.array((0, 6, 0, 1)),
                np.array((0, 7, 0, 1)),
                np.array((0, 8, 0, 1)),
                np.array((0, 9, 0, 1)),
                np.array((0, 10, 0, 1)),
                ]
        index = 0
        for gen_point in btg.linear_trajectory_gen(initial_position, final_position, steps):
            np.testing.assert_almost_equal(points[index], gen_point)
            index += 1

    def test_foward_z(self):
        initial_position = np.array((0, 0, 0, 1))
        final_position = np.array((0, 0, 10, 1))
        steps = 10
        points = [
                np.array((0, 0, 1, 1)),
                np.array((0, 0, 2, 1)),
                np.array((0, 0, 3, 1)),
                np.array((0, 0, 4, 1)),
                np.array((0, 0, 5, 1)),
                np.array((0, 0, 6, 1)),
                np.array((0, 0, 7, 1)),
                np.array((0, 0, 8, 1)),
                np.array((0, 0, 9, 1)),
                np.array((0, 0, 10, 1)),
                ]
        index = 0
        for gen_point in btg.linear_trajectory_gen(initial_position, final_position, steps):
            np.testing.assert_almost_equal(points[index], gen_point)
            index += 1


class TestBodyLinearTrajectoryGenerator(unittest.TestCase):
    def test_foward_y(self):
        initial_config = RobotConfiguration(
                                np.array((0, 0, 0, 1)),
                                np.array((0, 0, 0, 1)),
                                [0, 1, 2, 3, 4, 5],
                                [
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    ]
                                )
        final_config = RobotConfiguration(
                                np.array((0, 10, 0, 1)),
                                np.array((0, 0, 0, 1)),
                                [0, 1, 2, 3, 4, 5],
                                [
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    ]
                                )
        steps = 10
        points = [
                np.array((0, 1, 0, 1)),
                np.array((0, 2, 0, 1)),
                np.array((0, 3, 0, 1)),
                np.array((0, 4, 0, 1)),
                np.array((0, 5, 0, 1)),
                np.array((0, 6, 0, 1)),
                np.array((0, 7, 0, 1)),
                np.array((0, 8, 0, 1)),
                np.array((0, 9, 0, 1)),
                np.array((0, 10, 0, 1)),
                ]
        index = 0
        for gen_config in btg.body_linear_trajectory(initial_config,
                                                     final_config, steps):
            np.testing.assert_almost_equal(points[index], gen_config.body_position)
            np.testing.assert_almost_equal(np.array((0, 0, 0, 1)), gen_config.body_rotation)
            index += 1

    def test_rotation_ccw_z(self):
        initial_config = RobotConfiguration(
                                np.array((0, 0, 0, 1)),
                                np.array((0, 0, 0, 1)),
                                [0, 1, 2, 3, 4, 5],
                                [
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    ]
                                )
        final_config = RobotConfiguration(
                                np.array((0, 0, 0, 1)),
                                np.array((np.radians(2), 0, 0, 1)),
                                [0, 1, 2, 3, 4, 5],
                                [
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    ]
                                )
        steps = 10
        rotations = [
                np.array((1*np.radians(2)/10, 0, 0, 1)),
                np.array((2*np.radians(2)/10, 0, 0, 1)),
                np.array((3*np.radians(2)/10, 0, 0, 1)),
                np.array((4*np.radians(2)/10, 0, 0, 1)),
                np.array((5*np.radians(2)/10, 0, 0, 1)),
                np.array((6*np.radians(2)/10, 0, 0, 1)),
                np.array((7*np.radians(2)/10, 0, 0, 1)),
                np.array((8*np.radians(2)/10, 0, 0, 1)),
                np.array((9*np.radians(2)/10, 0, 0, 1)),
                np.array((10*np.radians(2)/10, 0, 0, 1)),
                ]
        index = 0
        for gen_config in btg.body_linear_trajectory(initial_config,
                                                     final_config, steps):
            np.testing.assert_almost_equal(np.array((0, 0, 0, 1)), gen_config.body_position)
            np.testing.assert_almost_equal(rotations[index], gen_config.body_rotation)
            index += 1


if __name__ == '__main__':
    unittest.main()
