import unittest
import numpy as np

from diloboderusabderus.hexapod_model import Hexapod
from diloboderusabderus.leg_model import Leg
from diloboderusabderus.robot_configuration import RobotConfiguration

import config as morphology_config


class TestHexapodPositionsRelative(unittest.TestCase):
    def setUp(self):
        self.hexapod = Hexapod(np.array((0, 0, 0, 1)), np.array((0, 0, 0, 1)), morphology_config)

    def test_legs_relative_position_self_position(self):
        absolute_position = []
        for leg in self.hexapod.legs:
            absolute_position.append(leg.position)
        expected = [np.array((0, 0, 0, 1))]*6

        for index, leg in enumerate(self.hexapod.legs):
            np.testing.assert_array_almost_equal(
                    leg.absolute_to_relative_position(
                                    absolute_position[index]),
                    expected[index])

    def test_leg_relative_position_body_center(self):
        absolute_position = [np.array((0, 0, 0, 1))]*6
        expected = [
            self.hexapod.legs[0].position * np.array([-1, -1, -1, 1]),
            self.hexapod.legs[1].position * np.array([-1, -1, -1, 1]),
            self.hexapod.legs[2].position * np.array([-1, -1, -1, 1]),
            self.hexapod.legs[3].position * np.array([1, 1, -1, 1]),
            self.hexapod.legs[4].position * np.array([1, 1, -1, 1]),
            self.hexapod.legs[5].position * np.array([1, 1, -1, 1])
            ]

        for index, leg in enumerate(self.hexapod.legs):
            np.testing.assert_array_almost_equal(
                    leg.absolute_to_relative_position(
                                    absolute_position[index]),
                    expected[index])


class TestHexapodPositionsAbsolute(unittest.TestCase):
    def setUp(self):
        self.hexapod = Hexapod(np.array((0, 0, 0, 1)), np.array((0, 0, 0, 1)), morphology_config)

    def test_legs_absolute_position_self_position(self):
        relative_position = [np.array([0, 0, 0, 1])]*6
        expected = []
        for leg in self.hexapod.legs:
            expected.append(leg.position)

        for index, leg in enumerate(self.hexapod.legs):
            np.testing.assert_array_almost_equal(
                    leg.relative_to_absolute_position(
                                    relative_position[index]),
                    expected[index])

    def test_leg_absolute_position_body_center(self):
        relative_position = [
            self.hexapod.legs[0].position * np.array([-1, -1, -1, 1]),
            self.hexapod.legs[1].position * np.array([-1, -1, -1, 1]),
            self.hexapod.legs[2].position * np.array([-1, -1, -1, 1]),
            self.hexapod.legs[3].position * np.array([1, 1, -1, 1]),
            self.hexapod.legs[4].position * np.array([1, 1, -1, 1]),
            self.hexapod.legs[5].position * np.array([1, 1, -1, 1])
            ]
        expected = [np.array([0, 0, 0, 1])]*6

        for index, leg in enumerate(self.hexapod.legs):
            np.testing.assert_array_almost_equal(
                    leg.relative_to_absolute_position(
                                    relative_position[index]),
                    expected[index])


class TestHexapodKinematics(unittest.TestCase):
    def setUp(self):
        self.hexapod = Hexapod(np.array((0, 0, 0, 1)), np.array((0, 0, 0, 1)), morphology_config)

    def test_inverse_kinematics_legs_extended(self):
        legs_lengths = self.hexapod.morphology_config.legs_lengths
        expected_angles = [[0, 0, 0]]*6

        body_position = np.array((0, 0, 0, 1))
        body_rotation = np.array((0, 0, 0, 1))
        support_pattern = [0, 1, 2, 3, 4, 5]
        legs_point_position = [np.array([legs_lengths[0] + legs_lengths[1] + legs_lengths[2], 0, 0, 1])]*6
        robot_config = RobotConfiguration(body_position, body_rotation, support_pattern, legs_point_position)

        angles = self.hexapod.inverse_kinematics(robot_config)
        np.testing.assert_almost_equal(angles, expected_angles)

    def test_inverse_kinematics_legs_90deg(self):
        legs_lengths = self.hexapod.morphology_config.legs_lengths
        expected_angles = [[0, np.pi/2, 0]]*6

        body_position = np.array((0, 0, legs_lengths[1] + legs_lengths[2], 1))
        body_rotation = np.array((0, 0, 0, 1))
        support_pattern = [0, 1, 2, 3, 4, 5]
        legs_point_position = [np.array([legs_lengths[0], 0, -(legs_lengths[1] + legs_lengths[2]), 1])]*6
        robot_config = RobotConfiguration(body_position, body_rotation, support_pattern, legs_point_position)

        angles = self.hexapod.inverse_kinematics(robot_config)
        np.testing.assert_almost_equal(angles, expected_angles)

    def test_inverse_kinematics_body_translated(self):
        legs_lengths = self.hexapod.morphology_config.legs_lengths
        expected_angles = [[0, np.pi/2, 0]]*6

        body_position = np.array((100, 0, (legs_lengths[1] + legs_lengths[2]), 1))
        body_rotation = np.array((0, 0, 0, 1))
        support_pattern = [0, 1, 2, 3, 4, 5]
        legs_point_position = [np.array([legs_lengths[0], 0, -(legs_lengths[1] + legs_lengths[2]), 1])]*6
        robot_config = RobotConfiguration(body_position, body_rotation, support_pattern, legs_point_position)

        angles = self.hexapod.inverse_kinematics(robot_config)
        np.testing.assert_almost_equal(angles, expected_angles)

    def test_inverse_kinematics_body_rotation(self):
        legs_lengths = self.hexapod.morphology_config.legs_lengths
        expected_angles = [[0, np.pi/2, 0]]*6

        body_position = np.array((0, 0, (legs_lengths[1] + legs_lengths[2]), 1))
        body_rotation = np.array((np.radians(10), 0, 0, 1))
        support_pattern = [0, 1, 2, 3, 4, 5]
        legs_point_position = [np.array([legs_lengths[0], 0, -(legs_lengths[1] + legs_lengths[2]), 1])]*6
        robot_config = RobotConfiguration(body_position, body_rotation, support_pattern, legs_point_position)

        angles = self.hexapod.inverse_kinematics(robot_config)
        np.testing.assert_almost_equal(angles, expected_angles)


class TestHexapodStability(unittest.TestCase):
    def setUp(self):
        self.hexapod = Hexapod(np.array((0, 0, 60, 1)), np.array((0, 0, 0, 1)), morphology_config)

    def test_hexapod_stability_all_support(self):
        body_height = (morphology_config.legs_lengths[1] + morphology_config.legs_lengths[2])*.7
        leg_point_relative_x = morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5
        central_legs_point_position = [
                np.array((
                    leg_point_relative_x,
                    0,
                    -body_height,
                    1)),
                np.array((
                    leg_point_relative_x,
                    0,
                    -body_height,
                    1)),
                np.array((
                    leg_point_relative_x,
                    0,
                    -body_height,
                    1)),
                np.array((
                    leg_point_relative_x,
                    0,
                    -body_height,
                    1)),
                np.array((
                    leg_point_relative_x,
                    0,
                    -body_height,
                    1)),
                np.array((
                    leg_point_relative_x,
                    0,
                    -body_height,
                    1)),
                ]
        test_config = RobotConfiguration(
                            body_position=self.hexapod.position.copy(),
                            body_rotation=self.hexapod.rotation.copy(),
                            support_pattern=[0, 1, 2, 3, 4, 5],
                            legs_point_position=central_legs_point_position
                            )
        stability = self.hexapod.stability(test_config)
        self.assertAlmostEqual(stability, leg_point_relative_x + morphology_config.body_config['body_width']/2)


if __name__ == '__main__':
    unittest.main()
