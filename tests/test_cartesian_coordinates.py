import unittest
import numpy as np

from diloboderusabderus.cartesian_coordinates import CartesianCoordinates


class TestCartesianCoordinatesCreateErrors(unittest.TestCase):
    def test_position_not_an_array(self):
        position = (0, 0, 0, 1)
        rotation = np.array((0, 0, 0, 1))
        with self.assertRaises(TypeError):
            CartesianCoordinates(position, rotation)

    def test_rotation_not_an_array(self):
        position = np.array((0, 0, 0, 1))
        rotation = (0, 0, 0, 1)
        with self.assertRaises(TypeError):
            CartesianCoordinates(position, rotation)

    def test_position_not_homogeneous(self):
        position = np.array((0, 0, 0, 0))
        rotation = np.array((0, 0, 0, 1))
        with self.assertRaises(ValueError):
            CartesianCoordinates(position, rotation)

    def test_rotation_not_normalized(self):
        position = np.array((0, 0, 0, 1))
        rotation = np.array((0, 0, 0, 0))
        with self.assertRaises(ValueError):
            CartesianCoordinates(position, rotation)


class TestCartesianCoordinatesErrors(unittest.TestCase):
    def setUp(self):
        position = np.array((0, 0, 0, 1))
        rotation = np.array((0, 0, 0, 1))
        self.cc = CartesianCoordinates(position, rotation)

    def test_absolute_not_an_array(self):
        not_an_array_position = (1, 0, 0, 1)
        self.assertRaises(TypeError,
                          self.cc.absolute_to_relative_position,
                          not_an_array_position)

    def test_relative_not_an_array(self):
        not_an_array_position = (1, 0, 0, 1)
        self.assertRaises(TypeError,
                          self.cc.relative_to_absolute_position,
                          not_an_array_position)

    def test_absolute_size_error(self):
        undersize_position = np.array((1, 0, 1))
        self.assertRaises(ValueError,
                          self.cc.absolute_to_relative_position,
                          undersize_position)

    def test_relative_size_error(self):
        undersize_position = np.array((1, 0, 1))
        self.assertRaises(ValueError,
                          self.cc.relative_to_absolute_position,
                          undersize_position)

    def test_absolute_not_homogeneous(self):
        not_homogeneous_position = np.array((1, 0, 0, 0))
        self.assertRaises(ValueError,
                          self.cc.absolute_to_relative_position,
                          not_homogeneous_position)

    def test_relative_not_homogeneous(self):
        not_homogeneous_position = np.array((1, 0, 0, 0))
        self.assertRaises(ValueError,
                          self.cc.relative_to_absolute_position,
                          not_homogeneous_position)


class TestCartesianCoordinates(unittest.TestCase):
    def test_cc_relative_position(self):
        position = np.array([15, 0, 0, 1])
        cases = [
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((15, 0, 0, 1))
                    },
                {
                    'position': np.array((10, 0, 0, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((5, 0, 0, 1))
                    },
                {
                    'position': np.array((0, 10, 0, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((15, -10, 0, 1))
                    },
                {
                    'position': np.array((0, 0, 10, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((15, 0, -10, 1))
                    },
                {
                    'position': np.array((10, 10, 10, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((5, -10, -10, 1))
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.pi/2, 0, 0, 1)),
                    'expected': np.array((0, -15, 0, 1))
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.pi/2, 0, 1, 0)),
                    'expected': np.array((0, 0, 15, 1))
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.pi/2, 1, 0, 0)),
                    'expected': np.array((15, 0, 0, 1))
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.pi, 0, 0, 1)),
                    'expected': np.array((-15, 0, 0, 1))
                    },
                {
                    'position': np.array((10, 10, 10, 1)),
                    'rotation': np.array((np.pi, 0, 0, 1)),
                    'expected': np.array((-5, 10, -10, 1))
                    },
                ]

        for index, case in enumerate(cases):
            with self.subTest(i=index):
                cc = CartesianCoordinates(case['position'], case['rotation'])
                relative_position = cc.absolute_to_relative_position(position)
                np.testing.assert_array_almost_equal(relative_position, case['expected'])

    def test_cc_absolute_position(self):
        position = np.array([15, 0, 0, 1])
        cases = [
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((15, 0, 0, 1))
                    },
                {
                    'position': np.array((10, 0, 0, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((25, 0, 0, 1))
                    },
                {
                    'position': np.array((0, 10, 0, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((15, 10, 0, 1))
                    },
                {
                    'position': np.array((0, 0, 10, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((15, 0, 10, 1))
                    },
                {
                    'position': np.array((10, 10, 10, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'expected': np.array((25, 10, 10, 1))
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.pi/2, 0, 0, 1)),
                    'expected': np.array((0, 15, 0, 1))
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.pi/2, 0, 1, 0)),
                    'expected': np.array((0, 0, -15, 1))
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.pi/2, 1, 0, 0)),
                    'expected': np.array((15, 0, 0, 1))
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.pi, 0, 0, 1)),
                    'expected': np.array((-15, 0, 0, 1))
                    },
                {
                    'position': np.array((10, 10, 10, 1)),
                    'rotation': np.array((np.pi, 0, 0, 1)),
                    'expected': np.array((-5, 10, 10, 1))
                    },
                ]

        for index, case in enumerate(cases):
            with self.subTest(i=index):
                cc = CartesianCoordinates(case['position'], case['rotation'])
                relative_position = cc.relative_to_absolute_position(position)
                np.testing.assert_array_almost_equal(relative_position, case['expected'])

    def test_relative_absolute_relative(self):
        cc = CartesianCoordinates(np.array((10, 15, 10, 1)), np.array((np.radians(10), 0, 0, 1)))
        position_on_test = np.array((100, 20, 14, 1))
        absolute_position = cc.relative_to_absolute_position(position_on_test)
        np.testing.assert_array_almost_equal(position_on_test, cc.absolute_to_relative_position(absolute_position))

    def test_absolute_relative_absolute(self):
        cc = CartesianCoordinates(np.array((10, 15, 10, 1)), np.array((np.radians(10), 0, 0, 1)))
        position_on_test = np.array((100, 20, 14, 1))
        relative_position = cc.absolute_to_relative_position(position_on_test)
        np.testing.assert_array_almost_equal(position_on_test, cc.relative_to_absolute_position(relative_position))


if __name__ == '__main__':
    unittest.main()
