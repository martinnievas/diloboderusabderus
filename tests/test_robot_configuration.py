import unittest
import numpy as np

from diloboderusabderus.robot_configuration import RobotConfiguration, robot_configuration_to_new_body_pose
from diloboderusabderus.hexapod_model import body_config


class TestRobotConfigurationErrors(unittest.TestCase):
    def test_position_not_an_array(self):
        position = (0, 0, 0, 1)
        rotation = np.array((0, 0, 0, 1))
        pattern = [1, 2, 3]
        legs_point_positions = [np.array((0, 0, 0, 1))]*6
        with self.assertRaises(TypeError):
            RobotConfiguration(position, rotation, pattern, legs_point_positions)

    def test_rotation_not_an_array(self):
        position = np.array((0, 0, 0, 1))
        rotation = (0, 0, 0, 1)
        pattern = [1, 2, 3]
        legs_point_positions = [np.array((0, 0, 0, 1))]*6
        with self.assertRaises(TypeError):
            RobotConfiguration(position, rotation, pattern, legs_point_positions)

    def test_position_not_homogeneous(self):
        position = np.array((0, 0, 0, 0))
        rotation = np.array((0, 0, 0, 1))
        pattern = [1, 2, 3]
        legs_point_positions = [np.array((0, 0, 0, 1))]*6
        with self.assertRaises(ValueError):
            RobotConfiguration(position, rotation, pattern, legs_point_positions)

    def test_rotation_not_normalized(self):
        position = np.array((0, 0, 0, 1))
        rotation = np.array((0, 0, 0, 0))
        pattern = [1, 2, 3]
        legs_point_positions = [np.array((0, 0, 0, 1))]*6
        with self.assertRaises(ValueError):
            RobotConfiguration(position, rotation, pattern, legs_point_positions)

    def test_pattern_not_list_or_tuple(self):
        position = np.array((0, 0, 0, 1))
        rotation = np.array((0, 0, 0, 1))
        pattern = None
        legs_point_positions = [np.array((0, 0, 0, 1))]*6
        with self.assertRaises(TypeError):
            RobotConfiguration(position, rotation, pattern, legs_point_positions)

    def test_pattern_not_empty(self):
        position = np.array((0, 0, 0, 1))
        rotation = np.array((0, 0, 0, 1))
        pattern = []
        legs_point_positions = [np.array((0, 0, 0, 1))]*6
        with self.assertRaises(ValueError):
            RobotConfiguration(position, rotation, pattern, legs_point_positions)


class TestRobotConfigurationToNewPose(unittest.TestCase):
    def test_to_body_translated_y(self):
        config = RobotConfiguration(
                                np.array((0, 0, 0, 1)),
                                np.array((0, 0, 0, 1)),
                                [0, 1, 2, 3, 4, 5],
                                [
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    ]
                                )
        new_config = robot_configuration_to_new_body_pose(config,
                                                          np.array((10, 0, 0, 1)),
                                                          np.array((0, 0, 0, 1)),
                                                          )

        expected_legs_point_position = [np.array((-10, 0, 0, 1)),
                                        np.array((-10, 0, 0, 1)),
                                        np.array((-10, 0, 0, 1)),
                                        np.array((10, 0, 0, 1)),
                                        np.array((10, 0, 0, 1)),
                                        np.array((10, 0, 0, 1))]

        for index, leg_point in enumerate(new_config.legs_point_position):
            np.testing.assert_array_almost_equal(leg_point, expected_legs_point_position[index])

    def test_body_position_change_xyz(self):
        config = RobotConfiguration(
                                np.array((0, 0, 0, 1)),
                                np.array((0, 0, 0, 1)),
                                [0, 1, 2, 3, 4, 5],
                                [
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    ]
                                )
        new_config = robot_configuration_to_new_body_pose(config,
                                                          np.array((10, 5, 1, 1)),
                                                          np.array((0, 0, 0, 1)),
                                                          )
        expected_legs_point_position = [np.array((-10, -5, -1, 1)),
                                        np.array((-10, -5, -1, 1)),
                                        np.array((-10, -5, -1, 1)),
                                        np.array((10, 5, -1, 1)),
                                        np.array((10, 5, -1, 1)),
                                        np.array((10, 5, -1, 1))]

        np.testing.assert_almost_equal(new_config.body_position, np.array((10, 5, 1, 1)))
        np.testing.assert_almost_equal(new_config.body_rotation, np.array((0, 0, 0, 1)))

        for index, leg_point in enumerate(new_config.legs_point_position):
            np.testing.assert_array_almost_equal(leg_point, expected_legs_point_position[index])

    def test_body_rotation_change_360(self):
        config = RobotConfiguration(
                                np.array((0, 0, 0, 1)),
                                np.array((0, 0, 0, 1)),
                                [0, 1, 2, 3, 4, 5],
                                [
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    ]
                                )
        new_config = robot_configuration_to_new_body_pose(config,
                                                          np.array((0, 0, 0, 1)),
                                                          np.array((np.radians(360), 0, 0, 1)),
                                                          )
        np.testing.assert_almost_equal(new_config.body_position, np.array((0, 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.body_rotation, np.array((np.radians(360), 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[0], np.array((0, 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[1], np.array((0, 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[2], np.array((0, 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[3], np.array((0, 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[4], np.array((0, 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[5], np.array((0, 0, 0, 1)))

    def test_body_rotation_change_180(self):
        config = RobotConfiguration(
                                np.array((0, 0, 0, 1)),
                                np.array((0, 0, 0, 1)),
                                [0, 1, 2, 3, 4, 5],
                                [
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    np.array((0, 0, 0, 1)),
                                    ]
                                )
        new_config = robot_configuration_to_new_body_pose(config,
                                                          np.array((0, 0, 0, 1)),
                                                          np.array((np.radians(180), 0, 0, 1)),
                                                          )
        np.testing.assert_almost_equal(new_config.body_position, np.array((0, 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.body_rotation, np.array((np.radians(180), 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[0],
                                       np.array((-body_config['body_width'], -body_config['body_length'], 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[1],
                                       np.array((-body_config['body_width'], 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[2],
                                       np.array((-body_config['body_width'], body_config['body_length'], 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[3],
                                       np.array((-body_config['body_width'], body_config['body_length'], 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[4],
                                       np.array((-body_config['body_width'], 0, 0, 1)))
        np.testing.assert_almost_equal(new_config.legs_point_position[5],
                                       np.array((-body_config['body_width'], -body_config['body_length'], 0, 1)))


if __name__ == '__main__':
    unittest.main()
